document.addEventListener('DOMContentLoaded', function() {
  initNavbar();
  setScrollToId();
  heroAnimation();
  initSliders();
  initParallax();
  initModals();
  initMapsToogler();
  initHotspots();
});

// onScroll effect to sticky navbar
const navbar = document.querySelector('nav.navbar');
const navbar_buttons = document.querySelectorAll('nav.navbar .menu-item');
window.onscroll = function() {
  if (window.pageYOffset >= 120) {
    navbar.classList.add('is-sticky');

    navbar_buttons.forEach(button => {
      button.classList.remove('is-v-yellow');
      button.classList.add('is-v-blue');
    });
  } else {
    navbar.classList.remove('is-sticky');

    navbar_buttons.forEach(button => {
      button.classList.remove('is-v-blue');
      button.classList.add('is-v-yellow');
    });
  }
}

function initNavbar() {
  // Get all "navbar-burger" elements
  const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {

    // Add a click event on each of them
    $navbarBurgers.forEach(el => {
      el.addEventListener('click', () => {

        // Get the target from the "data-target" attribute
        const target = el.dataset.target;
        const $target = document.getElementById(target);

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        el.classList.toggle('is-active');
        $target.classList.toggle('is-active');

      });
    });
  }
}

function setScrollToId() {
  document
    .querySelectorAll('a[href^="#"].menu-item')
    .forEach(trigger => {
      trigger.onclick = function(e) {
        e.preventDefault();
        let hash = this.getAttribute('href');
        let target = document.querySelector(hash);
        let headerOffset = 25;
        let elementPosition = target.offsetTop;
        let offsetPosition = elementPosition - headerOffset;

        window.scrollTo({
          top: offsetPosition,
          behavior: "smooth"
        });

        trigger.blur();
      };
    });
}

function initSliders() {
  new Splide('#home-slider', {
    type: 'fade',
    arrows: false,
    pagination: false,
    autoplay: true,
    speed: 800,
    interval: 6000,
    rewind: true,
    pauseOnFocus: false,
    pauseOnHover: false,
    fixedWidth: '100vw',
    fixedHeight: '100vh',
    cover: true,
  }).mount();

  new Splide('#street-art-slider', {
    type: 'loop',
    arrows: false,
    pagination: false,
    pauseOnFocus: false,
    pauseOnHover: false,
    autoplay: true,
    height: '300px',
    autoWidth: true,
    interval: 4000,
    speed: 1500,
  }).mount();

  new Splide('#hills-slider', {
    type: 'loop',
    arrows: true,
    pagination: true,
    pauseOnFocus: false,
    pauseOnHover: false,
    rewind: true,
    height: '300px',
    autoWidth: true,
    interval: 4000,
    speed: 2000,
  }).mount();

    new Splide('#funicular-slider', {
    type: 'loop',
    arrows: true,
    pagination: true,
    pauseOnFocus: false,
    pauseOnHover: false,
    height: '300px',
    autoWidth: true,
    interval: 4000,
    speed: 2000,
  }).mount();
}

function heroAnimation() {
  document.querySelector('#main-hero h1.title')
    .addEventListener('animationend', () => {
      document.querySelector('#main-hero h2.subtitle').style.visibility = 'visible';
      document.querySelector('#main-hero h2.subtitle').style.opacity = 1;
    });
}

function initParallax() {
  jarallax(document.querySelectorAll('.jarallax'), {
    speed: 0.3,
  });
}

function initModals() {
  document.querySelectorAll('button.modal-close').forEach(button => {
    button.addEventListener('click', function() {
      button.parentElement.classList.remove('is-active');
    });
  });

  document.querySelectorAll('.open-modal').forEach(button => {
    button.addEventListener('click', function(e) {
      e.preventDefault();
      document.getElementById(button.dataset.modal).classList.add('is-active');
    });
  });
}

function initMapsToogler() {
  var illustrated_map = document.querySelector('#illustrated-map img');
  var google_map = document.getElementById('g-map');

  document.querySelectorAll('#interactive-map div.toogle-maps button').forEach(button => {
    button.addEventListener('click', function() {
      button.classList.remove('is-outlined');
      for (let sibling of button.parentNode.children) {
        if (sibling !== button) sibling.classList.add('is-outlined');
      }
      if (button.dataset.action == 'show-gmap') {
        illustrated_map.parentNode.style.visibility = 'hidden';
        illustrated_map.parentNode.style.height = '0';
        google_map.style.visibility = 'visible';
        google_map.style.height = 'calc(90vh - 40px)';
        button.blur();
      } else {
        illustrated_map.parentNode.style.visibility = 'visible';
        illustrated_map.parentNode.style.height = 'auto';
        google_map.style.height = '0';
        google_map.style.visibility = 'hidden';
        button.blur();
      }
    });
  });
}

function initHotspots() {
  document.querySelectorAll('div.hotspot').forEach(spot => {
    spot.style.left = spot.dataset.x + "%";
    spot.style.bottom = spot.dataset.y + "%";
  });
}