const CITY_ID = "61328";
const API_KEY = "7Cl6nnw1sAbbDurGxiRGkhVXYGf1qkBe";
const URL_CURRENT = "https://dataservice.accuweather.com/currentconditions/v1/";
const URL_FORECAST = "https://dataservice.accuweather.com/forecasts/v1/daily/5day/"

document.addEventListener('DOMContentLoaded', function() {
  loadWeather();
  loadForecast();
});

function loadWeather() {

  var current_day = moment().tz('America/Santiago').format('dddd');
  var current_date = moment().tz('America/Santiago').format('MMM Do YYYY');
  var current_time = moment().tz('America/Santiago').format('hh:mm a [UTC] Z');

  $('footer div.weather-now h1.day').text(current_day);
  $('footer div.weather-now h1.date').text(current_date);
  $('footer div.weather-source p.source span.time').text(current_time);

  $.ajax({
      url: URL_CURRENT + CITY_ID,
      type: 'GET',
      dataType: 'json',
      data: { apikey: API_KEY, details: 'true' }
    })
    .done(function(data, textStatus, xhr) {
      if (xhr.status == '200') {
        fillWidget(data[0]);
      } else {
        console.log('Error en la solictud');
      }

    })
    .fail(function(data, textStatus, xhr) {
      $.getJSON('js/data_test/testDataWeather.json', function(data) {
        fillWidget(data[0]);
      });
    });
}

function loadForecast() {

  $.ajax({
      url: URL_FORECAST + CITY_ID,
      type: 'GET',
      dataType: 'json',
      data: { apikey: API_KEY, metric: 'true' }
    })
    .done(function(data, textStatus, xhr) {
      if (xhr.status == '200') {
        fillForecast(data);
      } else {
        console.log('Error en la solictud');
      }
    })
    .fail(function(data, textStatus, xhr) {
      $.getJSON('js/data_test/testDataForecast.json', function(data) {
        fillForecast(data);
      });
    });
}

function fillWidget(data) {
  var weather_icon = data.WeatherIcon;
  var temperature = Math.round(data.Temperature.Metric.Value);
  var weather_text = data.WeatherText;
  var precipitation = data.PrecipitationSummary.Precipitation.Metric.Value;
  var humidity = data.RelativeHumidity;
  var wind_speed = data.Wind.Speed.Metric.Value;
  var wind_direction = data.Wind.Direction.Localized;
  var source_url = data.Link;


  //setWeatherIcon(weather_icon, $('footer div.weather-now div.weather-icon i'));
  setWeatherIcon(weather_icon, $('footer div.sun i'));

  $('footer div.weather-now h1.temperature').text(temperature + ' °C');
  $('footer div.weather-now h1.weather-description').text(weather_text);
  $('footer div.weather-details p.weather-detail-data.detail-precipitation').text(precipitation + ' mm');
  $('footer div.weather-details p.weather-detail-data.detail-humidity').text(humidity + ' %');
  $('footer div.weather-details p.weather-detail-data.detail-wind').text(wind_direction + ' ' + wind_speed + ' km/h');
  $('footer div.weather-source p.source a')[0].href = source_url;
}

function fillForecast(data) {
  var weather_icon, day, temperature;
  $('footer div.weather-forecast > div').each( (index, element) => {
    weather_icon = data.DailyForecasts[index].Day.Icon;
    day = moment(data.DailyForecasts[index].Date).format('ddd');
    temperature = Math.round(data.DailyForecasts[index].Temperature.Maximum.Value);

    setWeatherIcon(weather_icon, $(element).find('div.weather-icon i'));
    $(element).find('p.day').text(day);
    $(element).find('p.temperature').text(temperature + '°C');
  });
}

function setWeatherIcon(icon_id, element) {
  switch (icon_id) {
    case 1:
      element.addClass('fa-sun');
      break;
    case 2:
      element.addClass('fa-sun-cloud');
      break;
    case 3:
      element.addClass('fa-cloud-sun');
      break;
    case 4:
      element.addClass('fa-cloud-sun');
      break;
    case 5:
      element.addClass('fa-sun-haze');
      break;
    case 6:
      element.addClass('fa-clouds-sun');
      break;
    case 7:
      element.addClass('fa-cloud');
      break;
    case 8:
      element.addClass('fa-clouds');
      break;
    case 11:
      element.addClass('fa-fog');
      break;
    case 12:
      element.addClass('fa-cloud-showers');
      break;
    case 13:
      element.addClass('fa-cloud-sun-rain');
      break;
    case 14:
      element.addClass('fa-cloud-sun-rain');
      break;
    case 15:
      element.addClass('fa-thunderstorm');
      break;
    case 16:
      element.addClass('fa-thunderstorm-sun');
      break;
    case 17:
      element.addClass('fa-thunderstorm-sun');
      break;
    case 18:
      element.addClass('fa-cloud-rain');
      break;
    case 19:
      element.addClass('fa-cloud-drizzle');
      break;
    case 20:
      element.addClass('fa-sun-dust');
      break;
    case 21:
      element.addClass('fa-sun-dust');
      break;
    case 22:
      element.addClass('fa-cloud-snow');
      break;
    case 23:
      element.addClass('fa-cloud-snow');
      break;
    case 24:
      element.addClass('fa-icicles');
      break;
    case 25:
      element.addClass('fa-cloud-sleet');
      break;
    case 26:
      element.addClass('fa-cloud-hail');
      break;
    case 29:
      element.addClass('fa-cloud-hail-mixed');
      break;
    case 32:
      element.addClass('fa-wind');
      break;
    case 33:
      element.addClass('fa-moon');
      break;
    case 34:
      element.addClass('fa-moon-cloud');
      break;
    case 35:
      element.addClass('fa-cloud-moon');
      break;
    case 36:
      element.addClass('fa-cloud-moon');
      break;
    case 37:
      element.addClass('fa-clouds-moon');
      break;
    case 38:
      element.addClass('fa-clouds-moon');
      break;
    case 39:
      element.addClass('fa-cloud-moon-rain');
      break;
    case 40:
      element.addClass('fa-cloud-moon-rain');
      break;
    case 41:
      element.addClass('fa-thunderstorm-moon');
      break;
    case 42:
      element.addClass('fa-thunderstorm-moon');
      break;
    case 43:
      element.addClass('fa-snowflakes');
      break;
    case 44:
      element.addClass('fa-snowflakes');
      break;
    default:
      element.addClass('fa-sun-cloud');
      break;
  }
}