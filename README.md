# IT817 Web Application Development #
## Assignment 2 ##

### Valparaíso website ###

* Site deployed using [Firebase](https://it817-testingmap.web.app/)
* [Source code](https://bitbucket.org/ehuenuman/valparaisoweb/src/master/public/)

#### Source code tree ####

These are the main files (coded by my own). The rest of files are libraries, also important, but with lots of code.

    public
    |-- index.html              (main HTML file)
    |-- css
    |   |-- index.css           (main CSS file that import /section files)
    |   |-- valparaiso.css      (CSS file created by valparaiso.scss using Bulma Framework)
    |   |-- valparaiso.scss     (SASS file to configure Bulma and create valparaiso.css automatically)
    |   `-- section             (CSS files for each website section)
    |       |-- nav.css
    |       |-- hero.css
    |       |-- intro.css
    |       |-- colour-houses.css
    |       |-- what-do.css
    |       |-- street-art-photos.css
    |       |-- interactive-map.css
    |       |-- video.css
    |       `-- footer.css
    |-- js                      (JS files)
    |   |-- index.js            (main JS file)
    |   |-- google_map.js       (Google Maps API)
    |   `-- weather_widget.js   (AccuWeather API)
    `-- src                     (Images used by the site)

#### Third-party services used ####

1. [Google Maps](https://developers.google.com/maps) API
2. [AccuWeather](https://developer.accuweather.com/apis) API
3. [YouTube](https://developers.google.com/youtube/iframe_api_reference) IFrame API
4. [Facebook](https://developers.facebook.com/docs/plugins) Social Plugin API

#### CSS libraries used ####

1. [Bulma Framework](https://bulma.io/) - CSS Framework
2. [Animate.css](https://animate.style/) - Pre-loaded CSS animations
3. [FontAwesome](https://fontawesome.com/) - Fonts and Icon toolkit

#### JS libraries used ####

1. [Splide](https://splidejs.com/) - JavaScript Slider
2. [Jarallax](https://free.nkdev.info/jarallax/) - Plain JS Parallax efect
3. [JQuery](https://jquery.com/) - Used to make the AJAX Requests
4. [Moment.js](https://momentjs.com/) - Parse, validate, manipulate, and display dates and times in JavaScript.
